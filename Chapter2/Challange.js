const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, data => {
            resolve(data);
        });
    });
}

async function main(){
    try {
        let arr = []
        lulus = []
        tidakLulus = []
        nilaiterbaik = []
        let ulang = true

        console.log(`Masukkan angka terlebuh dahulu,
Setelah itu Tekan q, 
lalu ENTER untuk melihat hasil.
Masukkan angka : `)
        while(ulang){
          let input = await question ("")
          nilai = (Number(input))
          if(input == 'q'){
            ulang = false
          } else {
              arr.push(nilai)
              ulang = true
                if (nilai >= 60){
                    lulus.push(nilai)
                } else {
                    tidakLulus.push(nilai)
                } if (nilai == 90 || nilai == 100){
                    nilaiterbaik.push(nilai)
                }
            }
        }
        function bubbleSort(arr) {
            for (let i = arr.length - 1; i >= 0; i--) {
              for (let j = 1; j <= i; j++) {
               
                if (arr[j - 1] > arr[j]) {
                  var temp = arr[j - 1];
                  arr[j - 1] = arr[j];
                  arr[j] = temp;
                }
              }
            }
          
            return arr;

          }
          function rata2(arr){
            total = 0
            for (i = 0; i < arr.length; i++) {
             total = total + arr[i]
             rata2 = total/arr.length
            }
           return rata2
          }

          console.log("Nilai tertinggi adalah : " +Math.max(...arr))
          console.log("Nilai terendah adalah : " +Math.min(...arr))
          console.log(`Nilai rata - rata adalah :`+rata2 (arr))
          console.log(`Siswa lulus : ${lulus}, Siswa tidak lulus : ${tidakLulus}`)
          console.log(`Nilai terendah ke tertinggi adalah :` +bubbleSort(arr))
          console.log(`Siswa nilai 90 dan nilai 100 : ${nilaiterbaik}`)
          interface.close();
        
    } catch (err) {
        console.log(err);
        interface.close();
        }
    }
    main();