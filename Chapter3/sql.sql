CREATE DATABASE Challange;

-- PEMASOK
CREATE TABLE Pemasok (
id_pemasok bigserial primary key,
nama_pemasok varchar(25) not null,
alamat text not null,
no_telp varchar (15) not null
);

insert into Pemasok (nama_pemasok, alamat, no_telp) values ('faris','kota_padang','081245789012');
insert into Pemasok (nama_pemasok, alamat, no_telp) values ('sarah','kota_jakarta','081545749782');

select * from Pemasok;
 id_pemasok | nama_pemasok |    alamat    |   no_telp
------------+--------------+--------------+--------------
          1 | faris        | kota_padang  | 081245789012
          2 | sarah        | kota_jakarta | 081545749782
(2 rows)

-- PRODUK
CREATE TABLE Produk (
id_produk bigserial primary key,
nama_produk varchar(25) not null,
stok int not null
);
CREATE TABLE

insert into produk (nama_produk, stok) values ('laptop', '150');

select * from produk;
 id_produk | nama_produk | stok
-----------+-------------+------
         1 | laptop      |  150
(1 row)

-- KOMPONEN
CREATE TABLE Komponen (
id_komponen bigserial primary key,
nama_komponen varchar(25) not null,
deskripsi text not null
);

insert into komponen (nama_komponen, deskripsi) values ('ram', 'penyimpanan_sementara');
insert into komponen (nama_komponen, deskripsi) values ('hardisk', 'penyimpanan_permanent');

select * from komponen;
 id | nama_komponen |       deskripsi
----+---------------+-----------------------
  1 | ram           | penyimpanan_sementara
  2 | hardisk       | penyimpanan_permanent
(2 rows)

-- TRANSACTION
CREATE TABLE transaction (
id bigserial primary key,
id_pemasok int,
id_komponen int
);

select * from transaction;
 id | id_pemasok | id_komponen
----+------------+-------------
(0 rows)

-- PENGHUBUNG
CREATE TABLE penghubung (
id bigserial primary key,
id_produk int,
id_komponen int
);

select * from penghubung;
 id | id_produk | id_komponen
----+-----------+-------------
(0 rows)

-- PROCEDURE TRANSACTION
create or replace procedure transaction ( id_pemasok int, id_komponen int)
language plpgsql
as $$
begin
insert into transaction ( id_pemasok, id_komponen) values (id_pemasok, id_komponen);
commit;
end;$$;

call transaction(1, 2);

select * from transaction;
 id | id_pemasok | id_komponen
----+------------+-------------
  1 |          1 |           2
(1 rows)

-- PROCEDURE PENGHUBUNG
create or replace procedure penghubung ( id_produk int, id_komponen int)
language plpgsql
as $$
begin
insert into penghubung ( id_produk, id_komponen) values (id_produk, id_komponen);
commit;
end;$$;

call penghubung (1, 2);
call penghubung (2, 2);

select * from penghubung;
 id | id_produk | id_komponen
----+-----------+-------------
  1 |         1 |           2
  2 |         2 |           2
(2 rows)