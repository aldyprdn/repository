const {User} = require('../db/models');
const {Supplier} = require('../db/models');
const {Component} = require('../db/models');
const {Product} = require('../db/models');

module.exports = {
    user: async () => {
        await User.destroy({truncate: true, restartIdentity: true});
    },
    Supplier: async () => {
        await Supplier.destroy({truncate: true, restartIdentity: true});
    },
    Component: async () => {
        await Component.destroy({truncate: true, restartIdentity: true});
    },
    Product: async () => {
        await Product.destroy({truncate: true, restartIdentity: true});
    }
};