const express = require('express');
const router = express.Router();
const user = require('../controllers/user');
const media = require('../controllers/media');
const notif = require('../controllers/notification');
const storage = require('../utils/strorage');
const multer = require('multer')();
const nodemailer = require('../utils/nodemailer');

const middlewares = require('../utils/middlewares');

const supplier = require('../controllers/supplier');
const component = require('../controllers/component');
const product = require('../controllers/product');

router.get('/', (req, res) => {
    return res.status(200).json({
        status: true,
        message: 'welcome to auth api!',
        data: null
    });
});

router.post('/auth/register', user.register);
router.post('/auth/login', user.login);
router.get('/auth/oauth', user.googleOauth2);
router.get('/auth/whoami', middlewares.auth, user.whoami);
router.get('/reset-password', user.resetPasswordPage);
router.post('/auth/forgot-password', user.forgotPassword);
router.post('/auth/reset-password', user.resetPassword);

router.post('/storage/images', storage.image.single('media'), media.strogeSingle);
router.post('/storage/multi/images', storage.image.array('media'), media.storageArray);
router.post('/imagekit/upload', multer.single('media'), media.imagekitUpload);

router.get('/notifications', middlewares.auth, notif.index);
router.put('/notifications/:id/read', middlewares.auth, notif.readNotif);

router.get('/', (req, res) => res.status(200).json({message: "welcome to blog api"}));

router.get('/suppliers', supplier.index); // get all supplier
router.get('/suppliers/:supplier_id', supplier.show); // get detail supplier
router.post('/suppliers', supplier.store); // create new supplier
router.put('/suppliers/:supplier_id', supplier.update); // update supplier
router.delete('/suppliers/:supplier_id', supplier.destroy); // delete supplier
router.post('/suppliers/connect', supplier.connect); // create new component_supplier

router.get('/components', component.index); // get all component
router.get('/components/:component_id', component.show); // get detail component
router.post('/components', component.store); // create new component
router.put('/components/:component_id', component.update); // update component
router.delete('/components/:component_id', component.destroy); // delete component

router.get('/products', product.index); // get all product
router.get('/products/:product_id', product.show); // get detail product
router.post('/products', product.store); // create new product
router.put('/products/:product_id', product.update); // update product
router.delete('/products/:product_id', product.destroy); // delete product
router.post('/products/connect', product.connect); // create new component_supplier



router.get('/test/mailer', async (req, res) => {
    try {
        // send email
        const html = await nodemailer.getHtml('welcome.ejs', {user: {name: 'Joko'}});
        nodemailer.sendMail('aldyprdn3@gmail.com', 'Ini Judul 3', html);

        return res.status(200).json({
            status: true,
            message: 'success',
            data: null
        });
    } catch (error) {
        throw error;
    }
});


module.exports = router;