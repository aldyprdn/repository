const {Supplier, Component_supplier}= require('../db/models')

module.exports = {
    index : async (req,res,next) => {
        try {
            const supplier = await Supplier.findAll();
            
            return res.status(200).json({
                status: true,
                message:'success',
                data: supplier
            });
        } catch (error) {
            next(error);
        }
    },
    store : async (req, res, next) => {
        try {
            const{name, address} = req.body;
            const ready = await Supplier.findOne({where: {name:name}})

            if(ready){
                return res.status(404).json({
                    status: false,
                    message:'supplier is already exist',
                    data: null
                });
            }
            const supplier = await Supplier.create({
                name: name,
                address: address   
             });
             
            return res.status(201).json({
                status: true,
                message:'success',
                data: supplier
            });
        } catch (error) {
            next(error);
        }
    },
    show: async (req,res,next)=>{
        try {
            const {supplier_id} = req.params;

            const supplier = await Supplier.findOne({
                where:{id:supplier_id}
            });

            if (!supplier){
                return res.status(401).json({
                    status: false,
                    message:`can't find supplier with id ${supplier_id}`,
                    data: null
                })
            }
            return res.status(200).json({
                status: true,
                message:'success',
                data: supplier
            });
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            const {supplier_id} = req.params;

            const updated = await Supplier.update(req.body, {where: {id: supplier_id}});

            if (updated[0] == 0) {
                return res.status(404).json({
                    status: false,
                    message: `can't find supplier with id ${supplier_id}`,
                    data: null
                });
            }

            return res.status(201).json({
                status: true,
                message: 'success',
                data: updated
            });
        } catch (error) {
            next(error);
        }
    },
    destroy: async (req, res, next) => {
        try {
            const {supplier_id} = req.params;

            const deleted = await Supplier.destroy({where: {id: supplier_id}});

            if (!deleted) {
                return res.status(404).json({
                    status: false,
                    message: `can't find supplier with id ${supplier_id}`,
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'success',
                data: deleted
            });
        } catch (error) {
            next(error);
        }
    },
    connect : async (req, res, next) => {
        try {
            const{id_component, id_supplier} = req.body;

            const component_supplier = await Component_supplier.create({
                id_component: id_component,
                id_supplier: id_supplier   
             });

            return res.status(201).json({
                status: true,
                message:'success',
                data: component_supplier
            });
        } catch (error) {
            next(error);
        }
    }
};