const {Product, Product_component}= require('../db/models')

module.exports = {
    index : async (req,res,next) => {
        try {
            const product = await Product.findAll();
            
            return res.status(200).json({
                status: true,
                message:'success',
                data: product
            });
        } catch (error) {
            next(error);
        }
    },
    store : async (req, res, next) => {
        try {
            const{name, quantity} = req.body;
            const ready = await Product.findOne({where: {name:name}})

            if(ready){
                return res.status(404).json({
                    status: false,
                    message:'product is already exist',
                    data: null
                });
            }
            const product = await Product.create({
                name: name,
                quantity: quantity   
             });

            return res.status(201).json({
                status: true,
                message:'success',
                data: product
            });
        } catch (error) {
            next(error);
        }
    },
    show: async (req,res,next)=>{
        try {
            const {product_id} = req.params;

            const product = await Product.findOne({
                where:{id:product_id}
            });

            if (!product){
                return res.status(401).json({
                    status: false,
                    message:`can't find product with id ${product_id}`,
                    data: null
                })
            }
            return res.status(200).json({
                status: true,
                message:'success',
                data: product
            });
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            const {product_id} = req.params;

            const updated = await Product.update(req.body, {where: {id: product_id}});

            if (updated[0] == 0) {
                return res.status(404).json({
                    status: false,
                    message: `can't find product with id ${product_id}`,
                    data: null
                });
            }

            return res.status(201).json({
                status: true,
                message: 'success',
                data: updated
            });
        } catch (error) {
            next(error);
        }
    },
    destroy: async (req, res, next) => {
        try {
            const {product_id} = req.params;

            const deleted = await Product.destroy({where: {id: product_id}});

            if (!deleted) {
                return res.status(404).json({
                    status: false,
                    message: `can't find product with id ${product_id}`,
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'success',
                data: deleted
            });
        } catch (error) {
            next(error);
        }
    },
    connect : async (req, res, next) => {
        try {
            const{id_component, id_product} =  req.body

            const product_component = await Product_component.create({
                id_product: id_product,
                id_component: id_component
            });

            return res.status(201).json({
                status: true,
                message:'success',
                data: product_component
            });
        } catch (error) {
            next(error);
        }
    }
};