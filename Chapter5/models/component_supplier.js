'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Component_supplier extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Component_supplier.init({
    id_component: DataTypes.INTEGER,
    id_supplier: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Component_supplier',
  });
  return Component_supplier;
};