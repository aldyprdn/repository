const {Component_supplier}= require('../models')

module.exports = {
    index : async (req,res,next) => {
        try {
            const component_supplier = await Component_supplier.findAll();
            
            return res.status(200).json({
                status: true,
                message:'success',
                data: component_supplier
            });
        } catch (error) {
            next(error);
        }
    },
    store : async (req, res, next) => {
        try {
            const{id_component, id_supplier} = req.body;

            const component_supplier = await Component_supplier.create({
                id_component: id_component,
                id_supplier: id_supplier   
             });

            return res.status(201).json({
                status: true,
                message:'success',
                data: component_supplier
            });
        } catch (error) {
            next(error);
        }
    },
    show: async (req,res,next)=>{
        try {
            const {component_supplier_id} = req.params;

            const component_supplier = await Component_supplier.findOne({
                where:{id:component_supplier_id}
            });

            if (!component_supplier){
                return res.status(401).json({
                    status: false,
                    message:`can't find component_supplier with id ${component_supplier_id}`,
                    data: null
                })
            }
            return res.status(200).json({
                status: true,
                message:'success',
                data: component_supplier
            });
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            const {component_supplier_id} = req.params;

            const updated = await Component_supplier.update(req.body, {where: {id: component_supplier_id}});

            if (updated[0] == 0) {
                return res.status(404).json({
                    status: false,
                    message: `can't find component_supplier with id ${component_supplier_id}!`,
                    data: null
                });
            }

            return res.status(201).json({
                status: true,
                message: 'success',
                data: null
            });
        } catch (error) {
            next(error);
        }
    },
    destroy: async (req, res, next) => {
        try {
            const {component_supplier_id} = req.params;

            const deleted = await Component_supplier.destroy({where: {id: component_supplier_id}});

            if (!deleted) {
                return res.status(404).json({
                    status: false,
                    message: `can't find component_supplier with id ${component_supplier_id}!`,
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'success',
                data: null
            });
        } catch (error) {
            next(error);
        }
    }
};