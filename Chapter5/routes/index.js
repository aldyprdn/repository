const express = require('express');
const router = express.Router();
const supplier = require('../controller/supplier');
const component_supplier = require('../controller/component_supplier');
const component = require('../controller/component');
const product_component = require('../controller/product_component');
const product = require('../controller/product');

router.get('/', (req, res) => res.status(200).json({message: "welcome to blog api"}));

router.get('/suppliers', supplier.index); // get all supplier
router.get('/suppliers/:supplier_id', supplier.show); // get detail supplier
router.post('/suppliers', supplier.store); // create new supplier
router.put('/suppliers/:supplier_id', supplier.update); // update supplier
router.delete('/suppliers/:supplier_id', supplier.destroy); // delete supplier

router.get('/component_suppliers', component_supplier.index); // get all component_supplier
router.get('/component_suppliers/:component_supplier_id', component_supplier.show); // get detail component_supplier
router.post('/component_suppliers', component_supplier.store); // create new component_supplier
router.put('/component_suppliers/:component_supplier_id', component_supplier.update); // update component_supplier
router.delete('/component_suppliers/:component_supplier_id', component_supplier.destroy); // delete component_supplier

router.get('/components', component.index); // get all component
router.get('/components/:component_id', component.show); // get detail component
router.post('/components', component.store); // create new component
router.put('/components/:component_id', component.update); // update component
router.delete('/components/:component_id', component.destroy); // delete component

router.get('/product_components', product_component.index); // get all product_component
router.get('/product_components/:product_component_id', product_component.show); // get detail product_component
router.post('/product_components', product_component.store); // create new product_component
router.put('/product_components/:product_component_id', product_component.update); // update product_component
router.delete('/product_components/:product_component_id', product_component.destroy); // delete product_component

router.get('/products', product.index); // get all product
router.get('/products/:product_id', product.show); // get detail product
router.post('/products', product.store); // create new product
router.put('/products/:product_id', product.update); // update product
router.delete('/products/:product_id', product.destroy); // delete product

module.exports = router;