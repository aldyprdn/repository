
const express = require('express');
const app = express();
const router = require('./routes');
const morgan = require('morgan');
const cors = require('cors');

const {
    HTTP_PORT = 5000
} = process.env;

app.use(express.json());
app.use(morgan('dev'));
app.use(cors());

app.use(router);

// 404 
app.use((req, res, next) => {
    return res.status(404).json({
        message: "404 Not Found!"
    });
});

// 500
app.use((err, req, res, next) => {
    return res.status(500).json({
        message: err.message
    });
});

app.listen(HTTP_PORT, () => console.log('running on port', HTTP_PORT));