const supertest = require('supertest');
const app = require('../app');
const truncate = require('../utils/truncate');

// reset database user
truncate.Supplier();
truncate.Component();
truncate.Product();

const supplier = {
    name: "Pt. Punda Pundi",
    address: "Jl. Berdikari No.38"
};

const component= {
    name: "Kancing",
    description: "Jenis Berlubang"
};

const product = {
    name: "Baju Renang",
    quantity: "2"
};

// post supplier 
// positive
describe('TEST post/supplier endpoint', () => {
    test('Post : success', async () => {
        try {

            const res = await supertest(app)
                .post('/suppliers')
                .send(supplier);

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('address');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });

// negative
    test('Post : not valid', async () => {
        try {

            const res = await supertest(app)
                .post('/suppliers')
                .send(supplier);

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe('supplier is already exist');
            
        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// post component
// positive
describe('TEST post/component endpoint', () => {
    test('Post : success', async () => {
        try {

            const res = await supertest(app)
                .post('/components')
                .send(component);

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('description');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });

// negative
    test('Post : not valid', async () => {
        try {

            const res = await supertest(app)
                .post('/components')
                .send(component);

                expect(res.statusCode).toBe(404);
                expect(res.body).toHaveProperty('status');
                expect(res.body).toHaveProperty('message');
                expect(res.body).toHaveProperty('data');
                expect(res.body.status).toBe(false);
            expect(res.body.message).toBe('component is already exist');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// post product
// positive
describe('TEST post/product endpoint', () => {
    test('Post : success', async () => {
        try {

            const res = await supertest(app)
                .post('/products')
                .send(product);

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('quantity');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });

// negative
    test('Post : not valid', async () => {
        try {

            const res = await supertest(app)
                .post('/products')
                .send(product);

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe('product is already exist');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// post component supplier
// positive
describe('TEST post/supplier endpoint', () => {
    test('Post : success', async () => {
        try {

            const res = await supertest(app)
                .post('/suppliers/connect')
                .send({
                        "id_component": "1",
                        "id_supplier": "1"
                    });

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('id_component');
            expect(res.body.data).toHaveProperty('id_supplier');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// post product component
// positive
describe('TEST post/product endpoint', () => {
    test('Post : success', async () => {
        try {

            const res = await supertest(app)
                .post('/products/connect')
                .send({
                        "id_product": "1",
                        "id_component": "1"
                    });

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('id_product');
            expect(res.body.data).toHaveProperty('id_component');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// supplier
// getAll
describe('TEST getAll endpoint', () => {
    test('getAll : Success', async () => {
        try {

            const res = await supertest(app)
                .get('/suppliers')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toEqual(expect.arrayContaining([expect.objectContaining({id:1})]));
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// getDetail
// positive
describe('TEST getDetail endpoint', () => {
    test('getDetail : Success', async () => {
        try {

            const res = await supertest(app)
            .get('/suppliers/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('address');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`getDetail : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .get(`/suppliers/${id}`)

            expect(res.statusCode).toBe(401);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find supplier with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// update
// positive
describe('TEST Update endpoint', () => {
    test('Update : Success', async () => {
        try {

            const res = await supertest(app)
            .put('/suppliers/1')
            .send(supplier)

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual([1]);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`Update : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .put(`/suppliers/${id}`)
            .send(supplier)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find supplier with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// component
// getAll
describe('TEST getAll endpoint', () => {
    test('getAll : Success', async () => {
        try {
            
            const res = await supertest(app)
                .get('/components')

                expect(res.statusCode).toBe(200);
                expect(res.body).toHaveProperty('status');
                expect(res.body).toHaveProperty('message');
                expect(res.body).toHaveProperty('data');
            expect(res.body.data).toEqual(expect.arrayContaining([expect.objectContaining({id:1})]));
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
            
        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// getDetail
// positive
describe('TEST getDetail endpoint', () => {
    test('getDetail : Success', async () => {
        try {

            const res = await supertest(app)
            .get('/components/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('description');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
test(`getDetail : id not valid`, async () => {
    try {
        
        const id = 100;
            const res = await supertest(app)
            .get(`/components/${id}`);

            expect(res.statusCode).toBe(401);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find component with id ${id}`);
            
        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// update
// positive
describe('TEST Update endpoint', () => {
    test('Update : Success', async () => {
        try {

            const res = await supertest(app)
            .put('/components/1')
            .send(component)

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual([1]);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`Update : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .put(`/components/${id}`)
            .send(component)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find component with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// product
// getAll
describe('TEST getAll endpoint', () => {
    test('getAll : Success', async () => {
        try {

            const res = await supertest(app)
                .get('/products')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toEqual(expect.arrayContaining([expect.objectContaining({id:1})]));
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// getDetail
// positive
describe('TEST getDetail endpoint', () => {
    test('getDetail : Success', async () => {
        try {

            const res = await supertest(app)
            .get('/products/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('id');
            expect(res.body.data).toHaveProperty('name');
            expect(res.body.data).toHaveProperty('quantity');
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`getDetail : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .get(`/products/${id}`)

            expect(res.statusCode).toBe(401);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find product with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// update
// positive
describe('TEST Update endpoint', () => {
    test('Update : Success', async () => {
        try {

            const res = await supertest(app)
            .put('/products/1')
            .send(product)

            expect(res.statusCode).toBe(201);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual([1]);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`Update : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .put(`/products/${id}`)
            .send(product)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find product with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});

// delete supplier
// positive
describe('TEST delete endpoint', () => {
    test('delete : Success', async () => {
        try {

            const res = await supertest(app)
            .delete('/suppliers/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual(1);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`delete : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .delete(`/suppliers/${id}`)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find supplier with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});
// delete component
// positive
describe('TEST delete endpoint', () => {
    test('delete : Success', async () => {
        try {

            const res = await supertest(app)
            .delete('/components/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual(1);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`delete : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .delete(`/components/${id}`)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find component with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});
// delete product
// positive
describe('TEST delete endpoint', () => {
    test('delete : Success', async () => {
        try {

            const res = await supertest(app)
            .delete('/products/1')

            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toStrictEqual(1);
            expect(res.body.status).toBe(true);
            expect(res.body.message).toBe('success');
        } catch (error) {
            expect(error).toBe('error');
        }
    });
// negative
    test(`delete : id not valid`, async () => {
        try {
            const id = 100
            const res = await supertest(app)
            .delete(`/products/${id}`)

            expect(res.statusCode).toBe(404);
            expect(res.body).toHaveProperty('status');
            expect(res.body).toHaveProperty('message');
            expect(res.body).toHaveProperty('data');
            expect(res.body.status).toBe(false);
            expect(res.body.message).toBe(`can't find product with id ${id}`);

        } catch (error) {
            expect(error).toBe('error');
        }
    });
});