const {Supplier} = require('../models');
const {Component} = require('../models');
const {Product} = require('../models');

module.exports = {
    Supplier: async () => {
        await Supplier.destroy({truncate: true, restartIdentity: true});
    },
    Component: async () => {
        await Component.destroy({truncate: true, restartIdentity: true});
    },
    Product: async () => {
        await Product.destroy({truncate: true, restartIdentity: true});
    }
};