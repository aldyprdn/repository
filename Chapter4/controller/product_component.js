const {Product_component}= require('../models');
const Product = require('./product');

module.exports = {
    index : async (req,res,next) => {
        try {
            const product_component = await Product_component.findAll();
            
            return res.status(200).json({
                status: true,
                message:'success',
                data: product_component
            });
        } catch (error) {
            next(error);
        }
    },
    store : async (req, res, next) => {
        try {
            const{id_component, id_product} =  req.body

            const product_component = await Product_component.create({
                id_product: id_product,
                id_component: id_component
            });

            return res.status(201).json({
                status: true,
                message:'success',
                data: product_component
            });
        } catch (error) {
            next(error);
        }
    },
    show: async (req,res,next)=>{
        try {
            const {product_component_id} = req.params;

            const product_component = await Product_component.findOne({
                where:{id:product_component_id}
            });

            if (!product_component){
                return res.status(401).json({
                    status: false,
                    message:`can't find product_component with id ${product_component_id}`,
                    data: null
                })
            }
            return res.status(200).json({
                status: true,
                message:'success',
                data: product_component
            });
        } catch (error) {
            next(error)
        }
    },
    update: async (req, res, next) => {
        try {
            const {product_component_id} = req.params;

            const updated = await Product_component.update(req.body, {where: {id: product_component_id}});

            if (updated[0] == 0) {
                return res.status(404).json({
                    status: false,
                    message: `can't find product_component with id ${product_component_id}!`,
                    data: null
                });
            }

            return res.status(201).json({
                status: true,
                message: 'success',
                data: null
            });
        } catch (error) {
            next(error);
        }
    },
    destroy: async (req, res, next) => {
        try {
            const {product_component_id} = req.params;

            const deleted = await Product_component.destroy({where: {id: product_component_id}});

            if (!deleted) {
                return res.status(404).json({
                    status: false,
                    message: `can't find product_component with id ${product_component_id}!`,
                    data: null
                });
            }

            return res.status(200).json({
                status: true,
                message: 'success',
                data: null
            });
        } catch (error) {
            next(error);
        }
    }
};