const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function question(q) {
    return new Promise(resolve => {
        interface.question(q, data => {
            resolve(data);
        });
    });
}

async function main() {
    try {
        let menu = await question(`
        Selamat datang di aplikasi kalkulator
        Pilih proses yang ingin dilakukan:
        1. Penjumlahan
        2. pengurangan
        3. Perkalian
        4. Pembagian
        5. Akar Kuadrat
        6. Luas Persegi
        7. Volume Kubus
        8. Volume Tabung
        9. Selesai (Tutup Program) 
        Kamu pilih yang mana: `)

        console.log(`Kamu pilih = ${menu}`);
        
        if (menu == 1){
            let input1 = await question('Masukkan angka pertama : ');
            let input2 = await question('Masukkan angka kedua : ');
            
            hasil = +input1 + +input2
            console.log(`Hasil penjumlahan adalah ${hasil}`)
            Keluar()
        }
        
        else if (menu == 2){
            let input1 = await question('Masukkan angka pertama : ');
            let input2 = await question('Masukkan angka kedua : ');
            hasil = +input1 - +input2
            console.log(`Hasil pengurangan adalah ${hasil}`)
            Keluar()
        }
            
        else if (menu == 3){
            let input1 = await question('Masukkan angka pertama : ');
            let input2 = await question('Masukkan angka kedua : ');
            hasil = +input1 * +input2
            console.log(`Hasil perkalian adalah ${hasil}`)
            Keluar()
        }
            
        else if (menu == 4){
            let input1 = await question('Masukkan angka pertama : ');
            let input2 = await question('Masukkan angka kedua : ');
            hasil = +input1 / +input2
            console.log(`Hasil pembagian adalah ${hasil}`)
            Keluar()
        }
            
        else if (menu == 5){
            let input = await question('Masukkan angka : ');
            hasil = Math.sqrt(+input)
            console.log(`Hasil akar kuadrat dari ${input} adalah ${hasil}`)
            Keluar()
        }
        
        else if (menu == 6){
            let sisi = await question('Masukkan sisi : ');
            hasil = +sisi * +sisi 
            console.log(`Luas persegi adalah ${hasil}`)
            Keluar()
        }

        else if (menu == 7){
            let rusuk = await question('Masukkan rusuk : ');
            hasil = +rusuk * +rusuk * +rusuk
            console.log(`Volume kubus adalah ${hasil}`)
            Keluar()
        }
        
        else if (menu == 8){
            let jari2 = await question('Masukkan jari jari : ');
            let tinggi = await question('Masukkan tinggi : ');
            hasil = 3.14 * +jari2 * +jari2 * +tinggi
            console.log(`Volume tabung adalah ${hasil}`)
            Keluar()
        }

        else if (menu == 9){
            interface.close();
        }
        
        
function Keluar(){
    interface.question(`Tekan 1 untuk kembali ke menu utama
Tekan ENTER untuk Tutup program :`,(menu)=>{
              pilihan = Number(menu)
              if (pilihan == 1){
                main.apply();
              }
              else {
              interface.close();
            }
          }
          
          )
          
            }

} catch (err) {
    console.log(err);
    interface.close();
    }
}
main();